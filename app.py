from flask import Flask, request, send_from_directory, redirect
import json
import prob2

# set the project root directory as the static folder, you can set others.
app = Flask(__name__, static_url_path='')

@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)

@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('css', path)

@app.route('/fonts/<path:path>')
def send_fonts(path):
    return send_from_directory('fonts', path)

@app.route("/page/<path:path>")
def index(path):
        return send_from_directory('page', path)

@app.route("/")
def main():
    return redirect("https://office-nkanish2002.c9.io/page/index.html")

@app.route("/api", methods = ["GET"])
def getData():
        food = request.args.get("food").lower()
        sports = request.args.get("sports").lower()
        music = request.args.get("music").lower()
        print [food, sports, music]
        data = prob2.findSentiment([food, sports, music])
        return json.dumps(data)

if __name__ == "__main__":
    app.run(debug=True, port = 8080, host = "0.0.0.0")
