import tweepy
import sys
import unicodedata
from textblob import TextBlob
import json
import urllib2
import requests
from geopy.geocoders import Nominatim

auth1 = tweepy.OAuthHandler("BpPS8FhAH2nhp2pIv6f8Okddf", "KPz4jUx6ZKTWgc0q7weoneaFcKmqJw9g18mwoQB8VRuq3F0BCM")
auth2 = tweepy.OAuthHandler("nfmy4ULLjyDfZQ4c9XUjzbcWq", "FCzUIG9VmwVwFgMN8V0Tnad9JNXImGPHrwD3zxuwpAWu2ak1Jg")
auth = auth1
auth.set_access_token("75303254-MBHUOonmN3mgoZCbGmYL3m9aiJprYFJGyUoggD5Bj", "BqSuRmTF05JZx3SHlFH8SGzIZNx5FMsXyG7YOQbfswsDz")

geo = Nominatim()

def getCountry(lat, lon):
    try:
        return geo.reverse((lat, lon)).address.split(',')[-1]
    except:
        print "country exp raised"
        return getCountry(lat, lon)

api = tweepy.API(auth)

def getCoords(country):
    places = api.geo_search(query=country, granularity="country")
    place = places[0] #can be imporoved by matching full name
    return place.bounding_box

def getCentroid(country):
    places = api.geo_search(query=country, granularity="country")
    place = places[0] #can be imporoved by matching full name
    place.centroid.append("3000km")
    return place.centroid 

class CustomStreamListener(tweepy.StreamListener):
    def on_status(self, status):
        try:
            self.json1 = status
            if status.geo != None:
                print(status.text)
                f = open("tweets3.csv","a")
                writeText = unicodedata.normalize('NFKD', status.text).encode('ascii','ignore')
                writeText = writeText.replace(",","")
                writeText = writeText.replace('\n','')
                writeText = writeText+","+getCountry(status.geo["coordinates"][0],status.geo["coordinates"][1])+status.geo["coordinates"][0]+status.geo["coordinates"][1]+"\n"
                print(writeText)
                f.write(writeText);
                f.close()
        except Error:
            print("raised")
            startStream()
        return True

    def on_error(self, status_code):
        print >> sys.stderr, 'Encountered error with status code:', status_code
        return True # Don't kill the stream

    def on_timeout(self):
        print >> sys.stderr, 'Timeout...'
        return True # Don't kill the stream

keywords = ["cricket", "ipl", "rcb", "rr", "csk", "kingsxi", "epl", "football", "rock", "hiphop" , "disco" , "gazals" , "bhajans" , "pop", "classical", "mexican", "indian", "punjabi", "pizza", "burger", "street", "masala", "chat", "tandoori", "cheese", "coffee", "tea", "beverages", "icecream", "cream", "deserts"]
def startStream():
    sapi = tweepy.streaming.Stream(auth, CustomStreamListener())
##    maxX = -200
##    minX = +200
##    minY = +200
##    maxY = -200
##    print(locations.coordinates[0])
##    for x, y in locations.coordinates[0]:
##        if minX > x:
##            minX = x
##        if maxX < x:
##            maxX = x
##        if minY > y:
##            minY = y
##        if maxY < y:
##            maxY = y
##    print minX
##    print maxX
##    print minY
##    print maxY
    sapi.filter(track=keywords)

#needs to be fixed
def search(locations):
##    print(locations.coordinates[0])
##    maxX = -200
##    minX = +200
##    minY = +200
##    maxY = -200
##    for x, y in locations.coordinates[0]:
##        if minX > x:
##            minX = x
##        if maxX < x:
##            maxX = x
##        if minY > y:
##            minY = y
##        if maxY < y:
##            maxY = y
##    print minX
##    print maxX
##    print minY
##    print maxY
    print (locations)
    return api.search(rpp = 100, q = "geocode:" + str(locations[1]) + "," + str(locations[0]) + "," + str(locations[2]))

def findSentiment(tags):
    f = open("tweets3.csv","r")
    sentences = f.readlines()
    f.close()
    polTuples = []
    for sentence in sentences:
        sentence = sentence.lower()
        tagPresent = False
        for tag in tags:
            if tag in sentence:
                tagPresent = True
                break
        if tagPresent == False:
            continue
        cols = sentence.split(",")
        sentence =  TextBlob(cols[0])
        polarity = sentence.sentiment.polarity
        polTuples.append((polarity,cols[1].replace("\n","") if len(cols) > 1 else ""))
    return formatOut(countryPolarity(polTuples))

def formatOut(outMap):
    print(outMap)
    outList = []
    for key, pos, nut, neg in outMap:
        ent = {}
        ent["country"] = key.title().lstrip()
        ent["posTweets"] = pos
        ent["nutTweets"] = nut
        ent["negTweets"] = neg
        outList.append(ent)
    return outList

def addToMap(dict1, key):
    if key in dict1.keys():
        dict1[key] += 1
    else:
        dict1[key] = 1

def countryPolarity(polTuples):
    positiveSenti = {}
    negativeSenti = {}
    neutralSenti = {}
    
    for s, cont in polTuples:
        if s > 0.3:
            addToMap(positiveSenti, cont)
        elif s < -0.3:
            addToMap(negativeSenti, cont)
        else:
            addToMap(neutralSenti, cont)
    return displayCont(positiveSenti, negativeSenti, neutralSenti)

def lookup(lat, lon):
    url = 'http://maps.googleapis.com/maps/api/geocode/json'
    params = {
        "latlng":"%s,%s" % (lat, lon),
        "sensor":"false"
        }
    print url
    resp = requests.get(url, params=params)
    data = {}
    if resp.status_code == 200:
        data = resp.json()
    else:
        raise Exception("Request to google maps failed")
    
    for result in data['results']:
        for component in result['address_components']:
            if 'country' in component['types']:
                return component['long_name']

    return None

def displayCont(positiveMap, negativeMap, neutralMap):
    list1 = sorted(positiveMap.items(), key=lambda x: x[1], reverse = True)

    contList = []

    for cont, positive_num in list1:
        nut_num = 0
        if cont in neutralMap.keys():
            nut_num = neutralMap[cont]
        neg_num = 0
        if cont in negativeMap.keys():
            neg_num = negativeMap[cont]
        contList.append((cont, positive_num, nut_num, neg_num))
    return contList

def covertLoction():
    f = open("tweets.csv","r")
    f1 = open("tweets3.csv", "w")
    lines = f.readlines()
    for line in lines:
        try:
            words = line.split(",")
            cont = getCountry(words[1],words[2].replace("\n", ""))
            print words[0]
            print cont
            f1.write(words[0]+","+cont+","+words[1]+","+words[2])
        except:
            continue
    f1.close()
    f.close()
